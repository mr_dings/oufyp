<?php
class Comment_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_comment($place_name){
        $query = $this->db->order_by('create_at', 'ASC')->get_where('places_comment', array('place_name' => $place_name));
        return $query->result_array();
    }
    
    public function insert_comment($data){
        $this->db->insert('places_comment', $data);
    }

}