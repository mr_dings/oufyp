<?php
class Event_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_events(){
        $query = $this->db->get('events');
        return $query->result_array();
    }

    public function get_event($keyword){
        $query = $this->db->get_where('events', array('name' => $keyword));
        return $query->result_array();

    }
}