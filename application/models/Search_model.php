<?php
require_once('vendor/autoload.php');

class Search_model extends CI_Model
{
    private $mongo;

    public function __construct()
    {
        $this->mongo = new MongoDB\Client("mongodb://localhost:27017");
        $this->load->database();
    }

    function conn($dbhost, $dbname, $dbuser, $dbpasswd)
    {
        $server = 'mongodb://' . $dbuser . ':' . $dbpasswd . '@' . $dbhost . '/' . $dbname;
        try {
            $conn = new MongoClient($server);
        $db = $conn->selectDB($dbname);
        } catch (MongoException $e) {
            throw new ErrorException('Unable to connect to db server. Error:' . $e->getMessage(), 31);
        }
        return $db;
    }

    function getOpenRice(){
        $query = $this->db->get('token');
        return $query->result_array();
    }

}