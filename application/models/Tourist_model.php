<?php
class Tourist_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_tourists(){
        $query = $this->db->get('tourists');
        return $query->result_array();
    }

    public function get_tourist($keyword){
        $query = $this->db->get_where('tourists', array('name' => $keyword));
        return $query->result_array();

    }
}