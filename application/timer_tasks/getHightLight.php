<?php
/**
 * Created by PhpStorm.
 * User: dings
 * Date: 27/1/2017
 * Time: 1:05 AM
 */

$mysqli = new mysqli('127.0.0.1', 'root', '', 'fyp');
if($mysqli->set_charset('utf-8')){
    printf("error loading character set");
}
$mysqli->query("SET NAMES utf8");
//只能用函数来判断是否连接成功
if(mysqli_connect_errno())
{
    die(mysqli_connect_error());
}

$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 2);//设置超时时间
$mysqli->real_connect('127.0.0.1', 'root', '', 'fyp');
$mysqli->query("SET NAMES utf8");

$url = "https://guide-apis.discoverhongkong.com/api/v1/GetPOIListM";

$headers = array(
    "Content-Type: application/json; charset=utf-8",
);

$ch = curl_init();
// "9d7bcb4b-a4e5-4279-a1ff-7ce7dd068e80" 必遊景點
// "89625645-d4a9-4231-b40b-34856ced00cc" 節日盛事
// "a3b93a0b-c7b2-4ff9-a854-814705f8e6a4" 文娛藝術
// "7ab5068c-ceae-45a4-bb47-07290c23b18e" 戶外探索
// "ab10d8ad-8c98-4949-b6f8-e9711046b6d7" 古今文化
// 吃喝
// "ec9304d5-f07c-4b7e-8b63-67f1fb9b4aa9", "e3ba5d70-102f-4296-b6cf-379413f49bac", "7d2701c5-2750-4be5-8751-4f2a3d2237a4", "9c6d56f7-978f-4146-b692-331127f6f920"
// "2ab2a546-aa24-4662-9860-eb732bc21da2" 購物
$fields = array(
    "SortBy" => "PoiName",
    "CategoryList" => ["9d7bcb4b-a4e5-4279-a1ff-7ce7dd068e80"],
    "Latitude" => "0.000000",
    "LCID" => "zh-HK",
    "Longitude" => "0.000000",
    "ListFrom" => 0,
    "ListSize" => 30,
    "Keyword"=> ""
);

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

$result = curl_exec($ch);
curl_close($ch);

$data = json_decode($result, true);
$list = $data['PoiList'];

$url_detail = "https://guide-apis.discoverhongkong.com/api/v1/GetPOIDetails";
$headers_detail = array(
    "Content-Type: application/json",
);

echo "<pre>";

$counter = 0;
$handle = fopen('tourists.csv', 'w');

foreach ($list as $key => $value) {

    echo $counter++ . "\n";

    $fields_detail = array(
        "PoiId" => $value['PoiId'],
        "PoiAddressId" => $value['PoiAddressId'],
        "LCID" => "zh-HK"
    );

    $currPoiName = strip_tags($value['PoiName']);

    if ($result = $mysqli->query("SELECT * FROM tourists where name = '{$currPoiName}' LIMIT 1")) {

        $num_of_rows = $result->num_rows;
//		echo $num_of_rows;
        if ($num_of_rows > 0) {
//			echo "found";
//			print_r($value);
            //			print_r($result->fetch_assoc());
            $result->close();
            continue;
        }
    }

    $ch_detail = curl_init();
    curl_setopt($ch_detail, CURLOPT_URL, $url_detail);
    curl_setopt($ch_detail, CURLOPT_POST, true);
    curl_setopt($ch_detail, CURLOPT_HTTPHEADER, $headers_detail);
    curl_setopt($ch_detail, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch_detail, CURLOPT_POSTFIELDS, json_encode($fields_detail));


    $result_detail = curl_exec($ch_detail);
    curl_close($ch_detail);
    $result_detail_arr = json_decode($result_detail, true);
    $result_detail = $result_detail_arr['PoiDetails'];

    $tourist['name'] = strip_tags($result_detail['Name']);
    $tourist['district'] = strip_tags($result_detail['DistrictName']);
    $tourist['address'] = strip_tags($result_detail['Address']);
    $tourist['description'] = "";
    $tourist['detail'] = strip_tags(trim($result_detail['MobileDescription']));
    $tourist['website'] = strip_tags($result_detail['WebsiteUrl']);
    $tourist['lat'] = strip_tags($result_detail['Latitude']);
    $tourist['lon'] = strip_tags($result_detail['Longitude']);
    $tourist['tel'] = strip_tags($result_detail['Tel']);
    $tourist['tag1'] = strip_tags($result_detail['CategoryName']);
    $tourist['tag2'] = "";
    $tourist['tag3'] = "";

    $url_image = insertToStr($result_detail['ThumbnailUrl'], 4, "s");

    $ch_image = curl_init();
    curl_setopt($ch_image, CURLOPT_URL, $url_image);
    curl_setopt($ch_image, CURLOPT_RETURNTRANSFER, true);

    $result_image = curl_exec($ch_image);
    curl_close($ch_image);

    $file_name = md5($url_image);
    $filename = $file_name .".jpg";

    $tp = @fopen("../image/" . $filename, "a");
    fwrite($tp, $result_image);
    fclose($tp);

    $tourist['imgSrc'] = "/api/getImage/" . $file_name . ".jpg";
    $tourist['openHour'] = 800;
    $tourist['endHour'] = 2300;
    $tourist['recommendMinute'] = 60;
    $stmt = $mysqli->prepare("INSERT INTO `tourists` VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param('ssssssddsssssiii',$tourist['name'],$tourist["district"],$tourist['address'],$tourist['description'],$tourist['detail'],$tourist['website'],$tourist['lat'],$tourist['lon'],$tourist['tel'],$tourist['imgSrc'],$tourist['tag1'], $tourist['tag2'], $tourist['tag3'] ,$tourist['openHour'],$tourist['endHour'],$tourist['recommendMinute']);
    $stmt->execute();
    $stmt->close();

//	if(!$mysqli->query($sql)){
//		echo $sql . '\n';
//		printf("Error: %s\n", $mysqli->error);
//	}

//	print_r($tourist);
}

fclose($handle);

function insertToStr($str, $i, $substr){
    //指定插入位置前的字符串
    $startstr="";
    for($j=0; $j<$i; $j++){
        $startstr .= $str[$j];
    }

    //指定插入位置后的字符串
    $laststr="";
    for ($j=$i; $j<strlen($str); $j++){
        $laststr .= $str[$j];
    }

    //将插入位置前，要插入的，插入位置后三个字符串拼接起来
    $str = $startstr . $substr . $laststr;

    //返回结果
    return $str;
}
