<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Comment_model');
		$this->load->helper('url_helper');
	}
	
	public function getComment($place_name){
		$place_name = urldecode($place_name);
		$data['comments'] = $this->Comment_model->get_comment($place_name);
		echo json_encode($data);
	}
	
	public function insertComment(){

		$c = file_get_contents('php://input');
		$j = json_decode($c, true);
		$data['comments'] = $this->Comment_model->insert_comment($j);
		echo 1;
	}
	
}
