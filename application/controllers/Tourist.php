<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tourist extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tourist_model');
		$this->load->model('Comment_model');
		$this->load->helper('url_helper');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
//		$data['tourists'] = $this->Tourist_model->get_tourists();
//		echo json_encode($data);
	}

	public function all(){
		$data['tourists'] = $this->Tourist_model->get_tourists();
		echo json_encode($data);
	}

	public function keyword($keyword){
		echo $keyword;
		$data['tourists'] = $this->Tourist_model->get_tourist($keyword);
		echo json_encode($data);
	}

	public function test(){
		echo "abc";
	}
	
}
