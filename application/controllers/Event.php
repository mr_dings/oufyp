<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('vendor/autoload.php');

class Event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Event_model');
		$this->load->helper('url_helper');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
//		$data['tourists'] = $this->Tourist_model->get_tourists();
//		echo json_encode($data);
	}

	public function all(){
		$data['events'] = $this->Event_model->get_events();
		echo json_encode($data);
	}

	public function keyword($keyword){
		echo $keyword;
		$data['events'] = $this->Event_model->get_event($keyword);
		echo json_encode($data);
	}

	public function test(){
		echo "abc";
	}
	
	public function add(){
		$data['success'] = $this->input->get('success', true);
		$this->load->view('add_event_from', $data);
	}

	public function addEvent(){
		$post = $this->input->post();
		foreach ($post as $key => $value){
			if(empty($value)){
				echo "some field is empty";
				return;
			}
		}
		$name = $post['name'];
		$description = $post['description'];
		$lat = doubleval($post['lat']);
		$lng = doubleval($post['lng']);
		$enddatetime = $post['date'] . ' ' . $post['time'];
		$endtimeMillis = strtotime($enddatetime) * 1000;

		$startdatetime = $post['start_data'] . ' ' . $post['start_time'];
		$starttimeMillis = strtotime($startdatetime) * 1000;


		$client = new MongoDB\Client("mongodb://localhost:27017");
		$collection = $client->project->event;
		$array = array(
			'loc' => [
				'type' => 'Point',
				'coordinates' => [$lng, $lat],
			],
			'name' => $name,
			'type' => 'event',
			'description' => $description,
			'durtime' => $endtimeMillis,
			'starttime' => $starttimeMillis
		);
		$collection->createIndex(array('loc' => '2dsphere'));
		$insertOneResult = $collection->insertOne($array);
		header('Location: ' . '/event/add?success=success');
	}
	
}
