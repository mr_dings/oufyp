<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('vendor/autoload.php');

class Search extends CI_Controller {

    private $client;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model('Search_model');
        $this->client = new MongoDB\Client("mongodb://localhost:27017");
        
    }

    public function index(){
        $this->search();
    }

    public function searchEvent(){
        $lat = $this->input->get('lat');
        $lng =  $this->input->get('lng');

        if(empty($lat) || empty($lng) || !is_numeric($lat) || !is_numeric($lng) || doubleval($lat) > 90){
            echo "fail";
            return;
        }

        $collection = $this->client->project->event;

        $query = array(
            'loc' => array(
                '$nearSphere' => array(
                    '$geometry' => array(
                        'type' => 'Point',
                        'coordinates' => array(doubleval($lng), doubleval($lat)),
                    ),
                    '$maxDistance' => 1*500
                )
            )
        );
        $result = $collection->find($query);
        $res["results"] = $result->toArray();
        header('Content-type: text/javascript');

        echo json_encode($res);
    }

    public function search(){

        $lat = $this->input->get('lat');
        $lng =  $this->input->get('lng');

        if(empty($lat) || empty($lng) || !is_numeric($lat) || !is_numeric($lng) || doubleval($lat) > 90){
            echo "fail";
            return;
        }

        $collection = $this->client->project->shop;

        $query = array(
            'loc' => array(
                '$nearSphere' => array(
                    '$geometry' => array(
                        'type' => 'Point',
                        'coordinates' => array(doubleval($lng), doubleval($lat)),
                    ),
                    '$maxDistance' => 1*500
                )
            )
        );
        $result = $collection->find($query);
        $res["results"] = $result->toArray();
        header('Content-type: text/javascript');

        echo json_encode($res);
//        echo "<pre>";
//        print_r(json_encode($res));
//        echo "</pre>";
    }
    
    public function searchFood(){
        $lat = $this->input->get('lat');
        $lng =  $this->input->get('lng');

        if(empty($lat) || empty($lng) || !is_numeric($lat) || !is_numeric($lng) || doubleval($lat) > 90){
            echo "fail";
            return;
        }

        $query = $this->db->get_where('token', array('name' => "openRice"));
//        $query = $this->db->get('token');
//        echo "<pre>";
//        print_r($query);
//        echo "</pre>";
        
//        exit(1);
        $count = 0;
        while ($count <= 1){
            $query = $this->db->get_where('token', array('name' => "openRice"));

            if($query->result_id->num_rows >= 1) {
                $data = $query->result_array();
            }else{
                return;
            }

            $token = $data[0]['token'];
            $refresh_token = $data[0]['refresh_token'];
//            echo $token . "<br/><br/>";
//            echo $refresh_token."<br/>";
            //"zfTYpo0B5Sx_K8bblemqQKPrrFd21YDJzKuV5WtYrYONwQqoCKe3Nga6jC29R8UJtKPvPTey11E2upaEvla7fw58uSeT_VgC3Yz2py_eSORa0Z3cw07-5TA7fvFsLgcpwTa6Mq1McxR--z3Ux2SteM6i1tW6Nc1uT25di72JGjKeXfpYxVhvj_x0vtaXMN60gOfmK22jXOW_w-VkdWt7z794Wk7fK3gbBld6HFgWhb8clyHtf4ByK2hjFhk96XIR8_6fF8MUOFvrHAHcMEGnlEfBOjm9QGv9RLTlnKxp5wbdBIiHPTjV8JJ5PeTz3JWXvNes7Q";
//            exit(1);
//            echo "connect to open rice";
            $data = self::requestOp($token, $lat, $lng);

//            echo "<pre>";
//            print_r($data);
//            echo "</pre>";

            $arrData = json_decode($data, true);

//            echo "<pre>";
//            print_r($arrData);
//            echo "</pre>";

            if(isset($arrData["message"])){
//                echo "oAuth fail";
                $count++;
                $tokenArr = self::AuthOp($refresh_token);
//                echo "<pre>";
//                print_r($tokenArr);
//                echo "</pre>";
                if(isset($tokenArr["access_token"]) && isset($tokenArr["refresh_token"])){
                    $updata = "update token set token = '{$tokenArr['access_token']}', refresh_token = '{$tokenArr['refresh_token']}' where name = 'openRice'";
                    $this->db->query($updata);
                }
            }else{
                echo $data;
                exit(1);
            }
//            echo "end";
        }

    }

    function requestOp($token, $lat, $lng){
        $url = "https://api.openrice.com/api/v3/search?geo=wgs84%2C{$lat}%2C{$lng}%2Cwgs84&regionId=0&rows=40&sortBy=Distance&startAt=0&suggestedCoupon=false&withinDistance=0.5";

//        echo $url;
        
        $headers = array(
            "Content-Type: application/json; charset=utf-8",
            "User-Agent OpenRice_iOS/5.5.4 (iPhone; iOS 10.0.2)",
            "Accept-Language: zh-HK",
            "Authorization: Bearer " . $token,
        );


        $ch_detail = curl_init();
        curl_setopt($ch_detail, CURLOPT_URL, $url);
        curl_setopt($ch_detail, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch_detail, CURLOPT_RETURNTRANSFER, true);


        $result = curl_exec($ch_detail);
        curl_close($ch_detail);

        return $result;
    }

    function AuthOp($refresh_token){
        $url = "https://api.openrice.com/oauth2/token";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Accept: */*",
            "Cookie: or_app_webview=1; or_app_deviceid=49336999-0988-4B1F-8436-D013BE4A3129; or_app_lang=zh-HK",
            "Connection: keep-alive",
            "X-OR-ImageProfileId: 1",
            "X-DeviceId: 49336999-0988-4B1F-8436-D013BE4A3129",
            "User-Agent: OpenRice_iOS/5.5.4 (iPhone; iOS 10.0.2)",
            "Accept-Language: zh-HK"
        );


//    $fields = array(
//        "client_id" => "orapp2015iphonev5",
//        "client_secret" => "OpenriceAuthTokenInAPICommon",
//        "grant_type" => "refresh_token",
//        "refresh_token" => $refresh_token
//    );

        $post_data = "client_id=orapp2015iphonev5&client_secret=OpenriceAuthTokenInAPICommon&grant_type=refresh_token&refresh_token={$refresh_token}";

        $ch_detail = curl_init();
        curl_setopt($ch_detail, CURLOPT_URL, $url);
        curl_setopt($ch_detail, CURLOPT_POST, true);
        curl_setopt($ch_detail, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch_detail, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_detail, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_detail, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($ch_detail);

        $data = json_decode($result, true);

        return $data;

        /*
        {
            "access_token": "RiQi7YqZziA8OMDywGjYDx6gTqb2HFW-MdH7Oj9dfYby0nPDPPfuZF30ScRZKU2IutP3ztkQmUCnK21aklTxWlW9lk2u6TFeg7PbDgRTZYY0vUVy56jzA-jasf0VXXvSoCNLjZoy-k6T8rh91r1RwN4-QlciGHkrcOJsKRq9BKna9XnWmlt3nwZbv_y4rWHTDIr_oQ6amNg3DgLL1N8jS5UJ9PtDzDf2ccN-gMFahL1JjYDLYBR33SVk3afT0bGJhsFeb6xH4AnDjwxsiKwO2yyu0AXpVxCF_DuShvJ0P3kUGEU9PfZ6nnLPtDxA3MeYXxFZrw",
            "token_type": "bearer",
            "expires_in": 14399,
            "refresh_token": "Rx6KcpvNeGMBGFu1-tegRsT0XNo-AmN8J9vI1hWwr1yszOWvE9pgE1KNQ5kNTyBtOXVaFVZfmwBciLi3SCbvPCmBGiRhOOoOYCMU-B6OWw8iEkmPZ61DNLHeUOar6SrlE3Eo9VTsRe-tmx1wr5B36HZyS13c3FbsQ62uS6WnH3YuJ5Gt_7RII1o5JxjurUVIta5rTiaNY2BOgNXP_HDHV3tEifberMA7RCXVr91B4xeH_i0GZokcM0DsDzRqr0OkgHETuiWsqenh9tRJ4g4XvEgLZ59dFYiF59yqdkWQSfbmX-P2-22Ie5tVzfiikUFYDeUBWw",
            ".issued": "Sat, 15 Apr 2017 19:07:44 GMT",
            ".expires": "Sat, 15 Apr 2017 23:07:44 GMT"
        }
        */
    }


}
