<!DOCTYPE html>
<html>
<head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
            width: 500px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        .warpper{
            width : 1000px;
            height: 80%;
            margin: 100px auto;
        }
        .left_content{
            padding-left: 30px;
            float: left;
            width: 400px;
        }
    </style>
</head>
<body>
<div class="warpper">
    <?php
        if($success != null){
            echo "insert data " . $success;
        }
    ?>
    <div class="left_content">
        <table>
            <form method="post" action="addEvent">
                <tr>
                    <th colspan="2"><h1>add event</h1></th>
                </tr>
                <tr>
                    <td><label for="name">name :</label></td>
                    <td><input type="text" value="name" name="name"/></td>
                </tr>
                <tr>
                    <td><label for="description">description :</label></td>
                    <td><input type="text" value="description" name="description"/></td>
                </tr>
                <tr>
                    <td><label for="datatime">start time</label></td>
                    <td><input type="date" name="start_date"/> <input type="time" name="start_time"/></td>
                </tr>
                <tr>
                    <td><label for="datatime">end time</label></td>
                    <td><input type="date" name="date"/> <input type="time" name="time"/></td>
                </tr>
                <tr>
                    <td>coord : </td>
                    <td><input id="lat" type="text" name="lat"/> <input id="lng" type="text" name="lng"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="submit"/></td>
                </tr>
            </form>
        </table>
    </div>
    <div id="map"></div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBohz0WNCHaOkyeF3QAANUgZ5Pdu9oABxs&callback=initMap"
            async defer></script>
    <script>
        var map;
        var marker;
        var old_marker;

        var lat_input = document.getElementById('lat');
        var lng_input = document.getElementById('lng');

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 22.316144, lng: 114.1781523},
                zoom: 15
            });

            marker = new google.maps.Marker({
                position: map.getCenter(),
                map: map,
                title: 'Hello World!'
            });

            lat_input.value = map.getCenter().lat();
            lng_input.value = map.getCenter().lng();


            map.addListener('center_changed', function() {
                // 3 seconds after the center of the map has changed, pan back to the
                // marker.
                var latLng = map.getCenter();
                console.log(latLng.lat() + ". " + latLng.lng());

                lat_input.value = latLng.lat();
                lng_input.value = latLng.lng();

                marker.setMap(null);
                marker = null;
                if(marker == null)
                    marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        title: 'Hello World!'
                    });
            });
        }

    </script>

</div>
</body>
</html>