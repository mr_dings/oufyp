<?php
require_once('../vendor/autoload.php');

/*
$url = "https://www.7fans.com.hk/7FANS.pl/?Language=zh_BigCN&SubAction=GetStoreList";

$headers = array(
    "Content-Type: application/json; charset=utf-8",
    "User-Agent 711 iPhone OS 10.0.2",
);


$ch_detail = curl_init();
curl_setopt($ch_detail, CURLOPT_URL, $url);
curl_setopt($ch_detail, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch_detail, CURLOPT_RETURNTRANSFER, true);


$result = curl_exec($ch_detail);
curl_close($ch_detail);


//$myfile = fopen("source/temp.json", "w");
//fwrite($myfile, $result);
*/
$result = file_get_contents("source/temp.json");
$data = json_decode($result, true);

$client = new MongoDB\Client("mongodb://localhost:27017");
$collection = $client->project->shop;

foreach($data["FF"]["Stores"] as $key => $place){
    if(intval($place['StoreLatitude']) > 90){
        $temp = $place['StoreLongitude'];
        $place['StoreLongitude'] = $place['StoreLatitude'];
        $place['StoreLatitude'] = $temp;
    }
    $array = array(
        'loc' => [
            'type' => 'Point',
            'coordinates' => [doubleval($place['StoreLongitude']), doubleval($place['StoreLatitude'])],
        ],
        'name' => "7-eleven",
        'type' => 'CVS',
        'address' => $place['StoreAddressDetail']
    );
    $collection->createIndex(array('loc' => '2dsphere'));
    $insertOneResult = $collection->insertOne($array);
}