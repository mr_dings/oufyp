<?php

require_once('../vendor/autoload.php');

$url = "https://loyaltyapi.circlek.hk/Branch/GetAll";

$headers = array(
    "phone_token: 45cfbb2e1ac33ecccf154447c63a25cf",
    "dev_token: 9E42B37D",
    "Content-Type: application/json; charset=utf-8",
    "User-Agent Circle%20K/2017.03.101940 CFNetwork/808.0.2 Darwin/16.0.0",
    "Accept-Language: zh-tw",
    "DeviceType: IOS",
);


$ch_detail = curl_init();
curl_setopt($ch_detail, CURLOPT_URL, $url);
curl_setopt($ch_detail, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch_detail, CURLOPT_RETURNTRANSFER, true);


$result = curl_exec($ch_detail);
curl_close($ch_detail);

$data = json_decode($result, true);

$client = new MongoDB\Client("mongodb://localhost:27017");
$collection = $client->project->shop;

foreach($data["Data"] as $key => $place){
    $array = array(
        'loc' => [
            'type' => 'Point',
            'coordinates' => [$place['longitude'], $place['latitude']],
        ],
        'name' => "circlek",
        'type' => 'CVS',
        'address' => $place['address_chi']
    );
    $collection->createIndex(array('loc' => '2dsphere'));
    $insertOneResult = $collection->insertOne($array);
}






