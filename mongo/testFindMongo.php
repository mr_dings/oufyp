<?php
require_once('../vendor/autoload.php');

$client = new MongoDB\Client("mongodb://localhost:27017");
$collection = $client->project->shop;

$query = array(
    'loc' => array(
        '$nearSphere' => array(
            '$geometry' => array(
                'type' => 'Point',
                'coordinates' => array(doubleval(113.96), doubleval(22.4)),
            ),
            '$maxDistance' => 1*1000
        )
    )
);
$res = $collection->find($query)->toArray();
print_r($res);