<?php

function requestOp($token){
    $url = "https://api.openrice.com/api/v3/search?geo=wgs84%2C22.313520%2C114.177669%2Cwgs84&regionId=0&rows=20&sortBy=Distance&startAt=0&suggestedCoupon=false&withinDistance=0.5";

    $headers = array(
        "Content-Type: application/json; charset=utf-8",
        "User-Agent OpenRice_iOS/5.5.4 (iPhone; iOS 10.0.2)",
        "Accept-Language: zh-HK",
        "Authorization: Bearer " . $token,
    );


    $ch_detail = curl_init();
    curl_setopt($ch_detail, CURLOPT_URL, $url);
    curl_setopt($ch_detail, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch_detail, CURLOPT_RETURNTRANSFER, true);


    $result = curl_exec($ch_detail);
    curl_close($ch_detail);

    $data = json_decode($result, true);

    return $data;
}

function AuthOp($refresh_token){
    $url = "https://api.openrice.com/oauth2/token";

    $headers = array(
        "Content-Type: application/x-www-form-urlencoded",
        "Accept: */*",
        "Cookie: or_app_webview=1; or_app_deviceid=49336999-0988-4B1F-8436-D013BE4A3129; or_app_lang=zh-HK",
        "Connection: keep-alive",
        "X-OR-ImageProfileId: 1",
        "X-DeviceId: 49336999-0988-4B1F-8436-D013BE4A3129",
        "User-Agent: OpenRice_iOS/5.5.4 (iPhone; iOS 10.0.2)",
        "Accept-Language: zh-HK"
    );


//    $fields = array(
//        "client_id" => "orapp2015iphonev5",
//        "client_secret" => "OpenriceAuthTokenInAPICommon",
//        "grant_type" => "refresh_token",
//        "refresh_token" => $refresh_token
//    );

    $post_data = "client_id=orapp2015iphonev5&client_secret=OpenriceAuthTokenInAPICommon&grant_type=refresh_token&refresh_token={$refresh_token}";

    $ch_detail = curl_init();
    curl_setopt($ch_detail, CURLOPT_URL, $url);
    curl_setopt($ch_detail, CURLOPT_POST, true);
    curl_setopt($ch_detail, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch_detail, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch_detail, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch_detail, CURLOPT_POSTFIELDS, $post_data);
    $result = curl_exec($ch_detail);

    $data = json_decode($result, true);

    return $data;

    /*
    {
        "access_token": "RiQi7YqZziA8OMDywGjYDx6gTqb2HFW-MdH7Oj9dfYby0nPDPPfuZF30ScRZKU2IutP3ztkQmUCnK21aklTxWlW9lk2u6TFeg7PbDgRTZYY0vUVy56jzA-jasf0VXXvSoCNLjZoy-k6T8rh91r1RwN4-QlciGHkrcOJsKRq9BKna9XnWmlt3nwZbv_y4rWHTDIr_oQ6amNg3DgLL1N8jS5UJ9PtDzDf2ccN-gMFahL1JjYDLYBR33SVk3afT0bGJhsFeb6xH4AnDjwxsiKwO2yyu0AXpVxCF_DuShvJ0P3kUGEU9PfZ6nnLPtDxA3MeYXxFZrw",
        "token_type": "bearer",
        "expires_in": 14399,
        "refresh_token": "Rx6KcpvNeGMBGFu1-tegRsT0XNo-AmN8J9vI1hWwr1yszOWvE9pgE1KNQ5kNTyBtOXVaFVZfmwBciLi3SCbvPCmBGiRhOOoOYCMU-B6OWw8iEkmPZ61DNLHeUOar6SrlE3Eo9VTsRe-tmx1wr5B36HZyS13c3FbsQ62uS6WnH3YuJ5Gt_7RII1o5JxjurUVIta5rTiaNY2BOgNXP_HDHV3tEifberMA7RCXVr91B4xeH_i0GZokcM0DsDzRqr0OkgHETuiWsqenh9tRJ4g4XvEgLZ59dFYiF59yqdkWQSfbmX-P2-22Ie5tVzfiikUFYDeUBWw",
        ".issued": "Sat, 15 Apr 2017 19:07:44 GMT",
        ".expires": "Sat, 15 Apr 2017 23:07:44 GMT"
    }
    */
}

$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 2);//设置超时时间
$mysqli->real_connect('127.0.0.1', 'root', '', 'fyp');

if($mysqli->errno){
    echo $mysqli->error;
    exit(1);
}

$sql = "select * from token where name = 'openRice'";

$count = 0;
while ($count <= 1){
    $rst = $mysqli->query($sql);

    if($rst->num_rows >= 1) {
        $data = $rst->fetch_assoc();
    }else{
        return;
    }

    $token = $data['token'];
    $refresh_token = $data['refresh_token'];
//    echo $token . "<br/>";
//    echo $refresh_token."<br/>";
    //"zfTYpo0B5Sx_K8bblemqQKPrrFd21YDJzKuV5WtYrYONwQqoCKe3Nga6jC29R8UJtKPvPTey11E2upaEvla7fw58uSeT_VgC3Yz2py_eSORa0Z3cw07-5TA7fvFsLgcpwTa6Mq1McxR--z3Ux2SteM6i1tW6Nc1uT25di72JGjKeXfpYxVhvj_x0vtaXMN60gOfmK22jXOW_w-VkdWt7z794Wk7fK3gbBld6HFgWhb8clyHtf4ByK2hjFhk96XIR8_6fF8MUOFvrHAHcMEGnlEfBOjm9QGv9RLTlnKxp5wbdBIiHPTjV8JJ5PeTz3JWXvNes7Q";

    echo "connect to open rice";
    $data = requestOp($token);

//    echo "<pre>";
//    print_r($data);
//    echo "</pre>";



//    exit(1);
//    return;
    if(isset($data["message"])){
        echo "oAuth fail";
        $count++;
        $tokenArr = AuthOp($refresh_token);
//        echo "<pre>";
//        print_r($tokenArr);
//        echo "</pre>";
        if(isset($tokenArr["access_token"]) && isset($tokenArr["refresh_token"])){
            $updata = "update token set token = '{$tokenArr['access_token']}', refresh_token = '{$tokenArr['refresh_token']}' where name = 'openRice'";
            $mysqli->query($updata);
        }
    }else{
        exit(1);
    }
    echo "end";
}
