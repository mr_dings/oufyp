<?php

require_once('../vendor/autoload.php');

$nameList = array();

for($j=1; $j<5; $j++){


    $resp = file_get_contents("source/wikiPlaze{$j}.html");

    $htmlDom = new DOMDocument();
    $htmlDom->strictErrorChecking = false;
    $source = @$htmlDom->loadHTML(mb_convert_encoding($resp, 'HTML-ENTITIES', 'UTF-8'));

    $finder = new DomXPath($htmlDom);
    $results = $finder->query("//table[@class='sortable prettytable']/tr[position()>1] | //table[@class='sortable wikitable']/tr[position()>1]");

    if ($results->length > 0){
        for($i=0; $i < $results->length; $i++){
            $tr = $results->item($i);
            $tdList = $tr->getElementsByTagName('td');
            $name = $tdList->item(0)->nodeValue;
            $name = str_replace(' ', '', $name);
            $name = str_replace("
", '', $name);
            if(substr($name, -3) == "）")
                $name = preg_replace("/（.*/", '', $name);
            if(substr($name, -1) == ")")
                $name = substr($name, 0, strpos($name, "("));
//            echo $name . '<br/>';
            array_push($nameList, $name);
//            echo $name . '<br/>';
    //        foreach ($tdList as $td) {
    //            echo "<pre>";
    //            print_r($td);
    //            $tender[$k][$j++] = trim($td->nodeValue);
    //            echo "</pre>";
    //        }
        }
    }
}
//echo "<pre>";
//print_r($nameList);
//echo "</pre>";
//exit(1);

$client = new MongoDB\Client("mongodb://localhost:27017");
$collection = $client->project->shop;

foreach ($nameList as $key => $name){
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$name}&key=AIzaSyC27uZ7RzXDaPMz2FQXaJrDhf13gkukC3o";
    
    $ch = curl_init();
    $headers = array(
        "accept-language: zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,zh-CN;q=0.2"
    );
    
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $resp = curl_exec($ch);
    curl_close($ch);
    
//    $geoJson = file_get_contents("source/geocode.json");
    $geoJson = json_decode($resp, true);
    //print_r($geoJson);
    //echo $geoJson['status'];
    if($geoJson['status'] != "OK"){
        echo "fail -> " . $name ." => $key \n";
    }else{
        $array = array(
            'loc' => [
                'type' => 'Point',
                'coordinates' => [$geoJson['results'][0]['geometry']['location']['lng'], $geoJson['results'][0]['geometry']['location']['lat']],
            ],
            'name' => $name,
            'type' => 'plaze',
            'address' => $geoJson['results'][0]['formatted_address']
        );
//        print_r($array);
        $collection->createIndex(array('loc' => '2dsphere'));
        $insertOneResult = $collection->insertOne($array);
    }
}